//
//  OTAU.m
//  OTAU
//
/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/
//
// Transfer the given Application image to the given device replacing the on Chip Application.
//
// OTAU Interface methods
//  - startOTAU
//  - abortOTAU
//
// OTAU delegates
//
//

#import "OTAU.h"
#import "ApplicationImage.h"
#import "CBPeripheral+CallbackQ.h"
#import "PCM.h"
#import "AppDelegate.h"
#import <CommonCrypto/CommonCrypto.h>


#define diagnostic173309    1

/****************************************************************************/
/*               Ble Delegate identifier                                    */
/****************************************************************************/
#define kDidDiscoverServices                @"didDiscoverServices"
#define kDidDiscoverCharacteristicsForService   @"didDiscoverCharacteristicsForService"
#define kDidUpdateNotificationStateForCharacteristic @"didUpdateNotificationStateForCharacteristic"
#define kDidWriteValueForCharacteristic     @"didWriteValueForCharacteristic"
#define kDidUpdateValueForCharacteristic    @"didUpdateValueForCharacteristic"
#define kPeripheralDidUpdateName            @"peripheralDidUpdateName"
#define kDidModifyServices                  @"didModifyServices"
#define kBleDidDiscoverPeripheral           @"BleDidDiscoverPeripheral"
#define kBleDidConnectPeripheral            @"BleDidConnectPeripheral"
#define kBleDidDiscoverServices             @"BleDidDiscoverServices"
#define kBleDidInvalidateServices           @"BleDidInvalidateServices"
#define kBleDidDisconnectPeripheral         @"BleDidDisconnectPeripheral"

/****************************************************************************/
/*              Private Methods and Properties                              */
/****************************************************************************/
@interface OTAU () {
    bool transferInProgress;
    NSInteger transferCount;
    NSInteger transferTotal;
    uint8_t transferPercent;
    bool delegateHit;
    bool otauRunning;
    bool didAbort;
    bool expectChallengeResponse;
    NSInteger   applicationNumber;
    bool waitForDisconnect;
    bool peripheralInBootMode;
    UInt8 otaVersion;
    AppDelegate *appDelegate;

#ifdef diagnostic173309
    int iDiagnostic173309;
#endif
    
}

@property (strong, nonatomic) CBPeripheral *targetPeripheral;
@property (strong, nonatomic) NSString *sourceFilename;
@property (strong, nonatomic) NSData *targetFilename;
@property (strong, nonatomic) NSData *crystalTrim, *btMacAddress, *iRoot, *eRoot;
@property (strong, nonatomic) NSDictionary* csKeyDb;
@property (strong, nonatomic) NSString* peripheralBuildId;
@end

/****************************************************************************/
/*              OTAU implementation                                         */
/****************************************************************************/

@implementation OTAU

@synthesize OTAUDelegate;
@synthesize targetPeripheral, sourceFilename, targetFilename;
@synthesize crystalTrim, btMacAddress, iRoot, eRoot;
@synthesize csKeyDb;
@synthesize peripheralBuildId;

/****************************************************************************/
/*								Initialise                                  */
/****************************************************************************/
// We expect there to be only the one instance hence the use of singleton
+ (id) sharedInstance {
    static OTAU	*this	= nil;
    
    if (!this)
        this = [[OTAU alloc] init];
    
    return this;
}


- (id) init {
    delegateHit = NO;
    otauRunning = NO;
    otaVersion = 0;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
#ifdef diagnostic173309
    iDiagnostic173309 = 0;
#endif

    return self;
}

/****************************************************************************/
/*								Interface                                   */
/****************************************************************************/
// Set the peripheral and discover the version and mode in a new thread.
// This must be called before reading cs keys and before calling startOTAU.
-(void) initOTAU:(CBPeripheral *) peripheral {
    targetPeripheral = peripheral;
    targetPeripheral.delegate = self;
    peripheralBuildId = nil;
    
    CBUUID *uuid = [CBUUID UUIDWithString:serviceApplicationOtauUuid];
    CBUUID *bl_uuid = [CBUUID UUIDWithString:serviceBootOtauUuid];
    
    for (CBService *service in targetPeripheral.services) {
        if ([service.UUID isEqual:uuid] || [service.UUID isEqual:bl_uuid]) {
            appDelegate.targetService = service;
            break;
        }
    }
    [[Discovery sharedInstance] setDiscoveryDelegate:self];
    
    [targetPeripheral deleteQueue];
    
    if (!otauRunning) {
        [self performSelectorInBackground:@selector(initMain) withObject:nil];
    }
}

-(void) initMain {
    [self discoverVersion];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [OTAUDelegate initComplete: otaVersion];
    }];
}

//===========================================================================
// Set up the OTAU process, start it in a new thread, then return to caller.
// The OTAU process is run in a seperate thread as it involves putting the thread to sleep
// while waiting for a callback.
//
//
-(void) startOTAU:(NSString *) firmwareFilepath
              and:(BOOL) challengeResponseEnabled {
    if (targetPeripheral == nil) {
        [self terminateOTAU:@"Failed: start OTAU" : 1018];
    }
    sourceFilename = firmwareFilepath;
    expectChallengeResponse = challengeResponseEnabled;
    applicationNumber = 1;
    transferInProgress = NO;
    didAbort = NO;
    [[Discovery sharedInstance] setDiscoveryDelegate:self];
    
    if (!otauRunning)
        [self performSelectorInBackground:@selector(otauMain) withObject:nil];
}

//=========================================================================
// Abort a running OTAU process.
// The background thread is stopped by sent a message telling it to terminate, rather than
// killing it as this allows the thread to clean up, resulting in a controlled and orderly thread kill.
//
-(void) abortOTAU:(CBPeripheral *) peripheral {
    peripheral.delegate = nil;
    
    // kill the backgound OTAU process
    otauRunning = NO;
    transferInProgress = NO;
    didAbort = YES;
    [[Discovery sharedInstance] setDiscoveryDelegate:nil];
}

//=========================================================================
// OTAU Terminated due to error, this is invoked upon a timeout or some unexpected error.
-(void) terminateOTAU:(NSString *) message :(int) errorCode {
    targetPeripheral.delegate = nil;
    
    otauRunning = NO;
    [[Discovery sharedInstance] setDiscoveryDelegate:nil];
    
    if (errorCode) {
        // Convert error code to 4 character string
        // THis will be used to lookup the Message string
        NSString *errorCodeString = [NSString stringWithFormat:@"%4d",(int)errorCode];
        
        // Lookup Error string from error code
        NSString *errorString = NSLocalizedStringFromTable (errorCodeString, @"ErrorCodes", nil);
        
        // Construct error object
        NSError *error = [[NSError alloc] initWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                    code:errorCode
                                                userInfo:@{errorCodeString: errorString} ];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [OTAUDelegate completed:[message capitalizedString] :error];
        }];
    }
    else
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [OTAUDelegate completed:[message capitalizedString] :nil];
        }];
}

//=========================================================================
// Parse and cache the cskey_db json file so that it can later be accessed by getCsKeyDbEntry.
-(bool) parseCsKeyJson : (NSString*) csKeyXmlFile {
    NSString* filePath = [[NSBundle mainBundle] pathForResource:csKeyXmlFile ofType:@"json"];
    NSError *error = nil;
    
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:&error];
    if (error != nil) {
        return NO;
    }
    // Create object from JSON data.
    csKeyDb = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingAllowFragments error:&error];
    if (error != nil) {
        return NO;
    }
    return YES;
}

//=========================================================================
// Look up a cs key db entry by build id and name. Must have previously called parseCsKeyJson.
-(NSDictionary*) getCsKeyDbEntry : (NSString*) buildId : (uint8_t) keyId {
    if (csKeyDb == nil) {
        return nil;
    }
    NSString* keyIdAsString = [NSString stringWithFormat:@"%d", keyId];
    NSArray *allBuilds = [[csKeyDb objectForKey: @"PSKEY_DATABASE"] objectForKey:@"PSKEYS"];
    for (NSDictionary *build in allBuilds) {
        if ([build[@"BUILD_ID"] isEqualToString:buildId]) {
            NSArray *keysForThisBuild = build[@"PSKEY"];
            for (NSDictionary *csKey in keysForThisBuild) {
                if ([csKey[@"ID"] isEqualToString: keyIdAsString]) {
                    return csKey;
                }
            }
        }
    }
    return nil;
}

/****************************************************************************/
/*				CoreBluetooth Peripheral delegates                          */
/****************************************************************************/

-(void) peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@"didDiscoverServices for peripheral %@",peripheral.name);
    PCM *pcm = [[PCM alloc] init:peripheral :nil :nil :error :kDidDiscoverServices];
    [peripheral saveCallback:pcm];
    delegateHit = YES;
}


- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    NSLog(@"didDiscoverCharacteristicsForService for peripheral %@ & Service %@",peripheral.name, service.UUID);
    PCM *pcm = [[PCM alloc] init:peripheral :service :nil :error :kDidDiscoverCharacteristicsForService];
    [peripheral saveCallback:pcm];
    delegateHit = YES;
}


- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"didUpdateNotificationStateForCharacteristic for peripheral %@ & Characteristic %@",peripheral.name, characteristic.UUID);
    PCM *pcm = [[PCM alloc] init:peripheral :nil :characteristic :error :kDidUpdateNotificationStateForCharacteristic];
    [peripheral saveCallback:pcm];
    delegateHit = YES;
}



- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (transferInProgress) {
        transferCount++;
        uint8_t percent = 50 + (50 * transferCount) / transferTotal;
        if (percent > transferPercent) {
            [OTAUDelegate didUpdateProgress: percent];
            transferPercent = percent;
        }
        if (transferCount == transferTotal) {
            transferInProgress = NO;
        }
    }

    NSLog(@"didWriteValueForCharacteristic for peripheral %@ & Characteristic %@",peripheral.name, characteristic.UUID);
    PCM *pcm = [[PCM alloc] init:peripheral :nil :characteristic :error :kDidWriteValueForCharacteristic];
    [peripheral saveCallback:pcm];
    delegateHit = YES;
    
#ifdef diagnostic173309
    if (iDiagnostic173309) {
        iDiagnostic173309 --;
        @synchronized (self) {
            NSString *str = [NSString stringWithFormat:@"** Did Write Response for %@\n",characteristic.UUID];
            [self statusMessage:str];
        }
        
    }
#endif

}


- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"didUpdateValueForCharacteristic for peripheral %@ & Characteristic %@ value=%@",peripheral.name, characteristic.UUID, characteristic.value);
    PCM *pcm = [[PCM alloc] init:peripheral :nil :characteristic :error :kDidUpdateValueForCharacteristic];
    [peripheral saveCallback:pcm];
    delegateHit = YES;
    
#ifdef diagnostic173309
    if (iDiagnostic173309) {
        iDiagnostic173309 --;
        @synchronized (self) {
            NSString *str = [NSString stringWithFormat:@"** Did Update Value for %@\n",characteristic.UUID];
            [self statusMessage:str];
        }
        
    }
#endif

}


-(void) peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray *)invalidatedServices {
    NSLog(@"didModifyServices peripheral %@",peripheral.name);
    [self statusMessage:@"Did Modify Service\n"];
    
}


- (void)peripheralDidUpdateName:(CBPeripheral *)peripheral {
    NSLog(@"peripheralDidUpdateName peripheral %@",peripheral.name);
}

-(void)peripheralDidInvalidateServices:(CBPeripheral *)peripheral {
    NSLog(@"peripheralDidInvalidateServices peripheral %@",peripheral.name);
}

/****************************************************************************/
/*								Discovery delegates                         */
/****************************************************************************/
// Reuse the DiscoveryDelegate singleton to route callbacks from CBCentral
- (void) didDiscoverPeripheral:(CBPeripheral *)peripheral {
    if ([peripheral.identifier isEqual:targetPeripheral.identifier])
    {
        NSLog(@"didDiscoverPeripheral peripheral %@",peripheral.name);
        PCM *pcm = [[PCM alloc] init:peripheral :nil :nil :nil :kBleDidDiscoverPeripheral];
        [peripheral saveCallback:pcm];
        delegateHit = YES;
    }
}


//============================================================================
- (void) didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"didConnectPeripheral peripheral %@",peripheral.name);
    PCM *pcm = [[PCM alloc] init:peripheral :nil :nil :nil :kBleDidConnectPeripheral];
    [peripheral saveCallback:pcm];
    delegateHit = YES;
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [OTAUDelegate didChangeConnectionState: true];
    }];
}


//============================================================================
-(void) didDisconnect:(CBPeripheral *)peripheral error:(NSError *)error {
    if (peripheral == targetPeripheral) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [OTAUDelegate didChangeConnectionState: false];
        }];
        NSLog(@"Target Peripheral Disconnect With Error = %@",error);
        if (waitForDisconnect==NO) {
            // Abort OTAU if the peripheral disconnected
            [self terminateOTAU:@"Failed: Peripheral Disconnected": OTAUErrorFailedDisconnected];
        }
        else
            waitForDisconnect = NO;
    }
    else
        NSLog(@"%@ peripheral Disconnected with error %@",peripheral.name, error);
    
}



/****************************************************************************/
/*								OTAU Helper Methods                         */
/****************************************************************************/

//============================================================================
// Wait for the given delegate to be called and if this occurs within 30 seconds then
// return TRUE otherwise False.
//
// One check a second is sufficient.
//
// Ignore callbacks other than given
//
-(PCM *) waitForDelegate:(NSString *) delegate {
    // Test every Second, timeout afer 30 seconds
    for (int i=0; i<30; i++) {
        [NSThread sleepForTimeInterval:1];
        if (targetPeripheral) {
            PCM *pcm = [targetPeripheral getCallback];
            if (pcm) {
                delegateHit=NO;
                if ([pcm.PCMName isEqualToString:delegate]) {
                    return (pcm);
                }
            }
        }
        if (otauRunning==NO)
            return(nil);
    }
    return (nil);
}

//============================================================================
// Discover Characteristics for given service
-(BOOL) discoverCharacteristics:(NSString *)serviceUUID {
    BOOL success=NO;
    [self statusMessage:@"Start: Discover Characteristics\n"];
    CBUUID *uuid = [CBUUID UUIDWithString:serviceUUID];
    for (CBService *service in targetPeripheral.services) {
        if ([service.UUID isEqual:uuid]) {
            delegateHit=NO;
            appDelegate.targetService = service;
            [targetPeripheral discoverCharacteristics:nil forService:service];
            PCM *pcm = [self waitForDelegate:kDidDiscoverCharacteristicsForService];
            if (pcm && (pcm.PCMError == NULL)) {
                [self statusMessage:@"Success: Discover Characteristics\n"];
                success = YES;
            }
            else {
                success = NO;
            }
        }
    }
    return (success);
}

//============================================================================
-(BOOL) createNotifyForCSKey {
    NSLog(@"otauCreateNotifyForCSKey");
    CBUUID *uuid;
    if ([appDelegate.peripheralInBoot boolValue])
        uuid = [CBUUID UUIDWithString:characteristicTransferControlUuid];
    else
        uuid = [CBUUID UUIDWithString:characteristicDataTransferUuid];
    
    
    
    for (CBCharacteristic *characteristic in appDelegate.targetService.characteristics) {
        
        if ([characteristic.UUID isEqual:uuid]) {
            delegateHit=NO;
            // Subscribe to Data transfer Char so we can be notified of CS key
            NSLog(@" -SetNotify for %@",characteristic.UUID);
            if (!characteristic.isNotifying) {
                [targetPeripheral setNotifyValue:YES forCharacteristic:characteristic];
            
                PCM *pcm = [self waitForDelegate:kDidUpdateNotificationStateForCharacteristic];
                if (pcm && ( pcm.PCMError == NULL ) )
                    return (YES);
                else
                    break;
            }
            else {
                NSLog(@"Notify already set");
                return (YES);
            }
        }
    }
    [self terminateOTAU:@"Failed: Subscribe for Notification": OTAUErrorFailedSubscribeForNotifications];
    return (NO);
    
}

//============================================================================
-(void) getCsKey: (uint8_t) csKeyIndex {
    if (!otauRunning) {
        [self performSelectorInBackground:@selector(getCsKeyMain:) withObject:[NSNumber numberWithUnsignedInt:csKeyIndex]];
    }
}

//============================================================================
-(void) getCsKeyMain : (NSNumber*) csKey {
    NSData *result = nil;
    uint8_t csKeyIndex = [csKey unsignedIntValue];
    
    NSString *str = [NSString stringWithFormat:@"getCsKeyMain: otauRunning=%d otauVersion=%d",otauRunning, otaVersion];
    [self statusMessage:str];

    if (!otauRunning && targetPeripheral && otaVersion >= 4) {
        otauRunning = YES;
        
        do {
            if (targetPeripheral.state != CBPeripheralStateConnected) {
                if (![self connect:serviceApplicationOtauUuid]) {
                    break;
                }
            }
            
            targetPeripheral.delegate = self;
            
            bool useLegacy = NO;
            if ([appDelegate.peripheralInBoot boolValue]) {
                if (otaVersion >= 5) {
                    // If in bootloader and v5 then use getCsKeyLegacy (if index in range).
                    useLegacy = YES;
                }
                else {
                    // If in bootloader and v4 then no way to get keys.
                    break;
                }
            }
            else {
                if (otaVersion == 4) {
                    // If in bootloader and v5 then use getCsKeyLegacy (if index in range).
                    useLegacy = YES;
                }
                // Otherwise we will use getCsKeyFromBlock
            }
            if (useLegacy) {
                [self statusMessage:@"using Legacy cskey method"];
                if ([self createNotifyForCSKey]) {
                    PCM *pcm = [self getCsKeyLegacy:(otaVersion >= 5) : [appDelegate.peripheralInBoot   boolValue] : csKeyIndex];
                    if (pcm && (pcm.PCMError == NULL)) {
                        result = pcm.PCMCharacteristic.value;
                    }
                }
            }
            else {
                [self statusMessage:@"using Normal cskey method"];
                if ([self createNotifyForCSKey]) {
                    result = [self getCsKeyFromBlock:csKeyIndex];
                }
                else {
                    [self statusMessage: @"Failed to register for cs key notification"];
                }
            }
            break;
        } while(0);
    }
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [OTAUDelegate didGetCsKeyValue: result];
    }];
    otauRunning = NO;
}

//============================================================================
-(PCM *) getCsKeyLegacy :(BOOL) isOTAUv5 :(BOOL) isBootMode :(uint8_t) csKeyIndex {
    PCM *retVal = NULL;
    
    CBUUID *getCsKeyCharUuid = [CBUUID UUIDWithString:characteristicGetKeysUuid];
    
    CBUUID *dataTransferCharUuid = [CBUUID UUIDWithString:characteristicDataTransferUuid];
    
    CBCharacteristic *dataTransferCharacteristic = NULL;
    CBCharacteristic *getCsKeyCharacteristic = NULL;
    
    for (CBCharacteristic *characteristic in appDelegate.targetService.characteristics) {
        if ([characteristic.UUID isEqual:dataTransferCharUuid]) {
            // Is the data transfer characterisitic found, update pointer
            dataTransferCharacteristic = characteristic;
        }
        else if ([characteristic.UUID isEqual:getCsKeyCharUuid]) {
            // Is the Read CsKey characteristic discovered
            getCsKeyCharacteristic = characteristic;
        }
        if ( getCsKeyCharacteristic && dataTransferCharacteristic ) {
            // If both the required characterisitics have been discovered
            delegateHit=NO;
            // Ask for the key specified in the index.
            NSData *csCommand = [NSData dataWithBytes:(void *)&csKeyIndex
                                               length:sizeof(csKeyIndex)];
            [targetPeripheral writeValue:csCommand
                       forCharacteristic:getCsKeyCharacteristic
                                    type:CBCharacteristicWriteWithResponse];
            if ( isOTAUv5 && isBootMode ) {
                PCM *pcm = [self waitForDelegate:kDidWriteValueForCharacteristic];
                if (pcm == nil) {
                    [self statusMessage: @"Failed: No write response requesting cs key from boot loader\n"];
                    break;
                }
                /* OTAU v5 bootloader doesn't support notifications on data transfer
                 characterisitic. We need to read the value */
                [targetPeripheral readValueForCharacteristic:dataTransferCharacteristic];
            }
            if (otauRunning) {
                NSLog(@"OTAU is running");
            }
            else {
                NSLog(@"OTAU is not running");
            }
            retVal = [self waitForDelegate:kDidUpdateValueForCharacteristic];
            break;
        }
    }
    
    return retVal;
}

//============================================================================
// Get any cs key using the read from block characteristic.
// Must have previously called parseCsKeyJson to load the cs key database.
// OTAv5 application only. Not supported in bootloader and not supported in v4.
-(NSData *) getCsKeyFromBlock : (uint8_t) csKeyId {
    PCM *pcm = NULL;
    
    CBUUID *getCsBlockUuid = [CBUUID UUIDWithString:characteristicGetKeyBlockUuid];
    CBUUID *dataTransferCharUuid = [CBUUID UUIDWithString:characteristicDataTransferUuid];
    
    CBCharacteristic *dataTransferCharacteristic = NULL;
    CBCharacteristic *csBlockCharacteristic = NULL;
    
    for (CBCharacteristic *characteristic in appDelegate.targetService.characteristics) {
        if ([characteristic.UUID isEqual:dataTransferCharUuid]) {
            // Is the data transfer characterisitic found, update pointer
            dataTransferCharacteristic = characteristic;
        }
        else if ([characteristic.UUID isEqual:getCsBlockUuid]) {
            // Is the Read CsKey characteristic discovered
            csBlockCharacteristic = characteristic;
        }
        if ( csBlockCharacteristic && dataTransferCharacteristic ) {
            // If both the required characterisitics have been discovered
            delegateHit=NO;
            
            // First get build id if we don't have it already
            // (this is reset to nil every time init is called after connecting to a new device).
            if (peripheralBuildId == nil) {
                NSLog(@"Getting build id");
                Byte bytes []  = { 0x00, 0x00, 0x02, 0x00 };
                NSMutableData *buildIdCommand = [NSMutableData dataWithBytes:bytes length:4];
                
                [targetPeripheral writeValue:buildIdCommand
                           forCharacteristic:csBlockCharacteristic
                                        type:CBCharacteristicWriteWithResponse];
                
                pcm = [self waitForDelegate:kDidWriteValueForCharacteristic];
                
                if (pcm == nil) {
                    [self statusMessage:@"Failed: no write response when trying to read build id\n"];
                    break;
                }
                else {
                    NSLog(@"Requested build id ok");
                }
                
                pcm = [self waitForDelegate:kDidUpdateValueForCharacteristic];
                
                if (pcm && (pcm.PCMError == NULL)) {
                    NSData *data = [NSData dataWithData: pcm.PCMCharacteristic.value];
                    uint16_t *build = (uint16_t *)[data bytes];
                    if (build!=nil) {
                        peripheralBuildId = [NSString stringWithFormat:@"%d", *build];
                        NSLog(@"Build id is %@", peripheralBuildId);
                    }
                    pcm = nil;
                }
            }
            
            if (peripheralBuildId != nil) {
                NSDictionary *csKeyEntry = [self getCsKeyDbEntry:peripheralBuildId :csKeyId];
                NSLog(@"Cs key entry is %@", csKeyEntry);
                if (csKeyEntry != nil) {
                    
                    uint16_t offset = [csKeyEntry[@"OFFSET"] intValue];
                    uint16_t lenBytes = [csKeyEntry[@"LENGTH"] intValue] * 2;

                    NSMutableData *csCommand = [[NSMutableData alloc] init];
                    [csCommand appendBytes:(void *)&offset length:sizeof(offset)];
                    [csCommand appendBytes:(void *)&lenBytes length:sizeof(lenBytes)];
                    
#ifdef diagnostic173309
                    NSString *str = [NSString stringWithFormat:@"** Write csCommand %@\n",csCommand];
                    [self statusMessage:str];
                    iDiagnostic173309 = 2;
#endif
                    
                    
                    [targetPeripheral writeValue:csCommand
                               forCharacteristic:csBlockCharacteristic
                                            type:CBCharacteristicWriteWithResponse];
    
                    pcm = [self waitForDelegate:kDidUpdateValueForCharacteristic];
                }
                else {
                    [self statusMessage:@"Failed: Build id or cs key id not found in cs key db\n"];
                }
            }
            else {
                [self statusMessage:@"Failed: Could not read build id\n"];
            }
            break;
        }
    }
    
    NSData *result = NULL;
    if (pcm && (pcm.PCMError == NULL)) {
        result = [NSData dataWithData: pcm.PCMCharacteristic.value];
    }
    else if (pcm) {
        NSLog(@"Error is %@", pcm.PCMError.localizedDescription);
    }
    return result;
}

//============================================================================
-(BOOL) getBtMacAddress :(BOOL) isOTAUv5 :(BOOL) isBootMode {
    
    PCM *pcm = [self getCsKeyLegacy:isOTAUv5:isBootMode:1];
    if (pcm && (pcm.PCMError == NULL)) {
        if ( !isOTAUv5 ) {
            btMacAddress = [pcm.PCMCharacteristic.value subdataWithRange:NSMakeRange(0, 6)];
        }
        else {
            Byte b[] = { 0, 0, 0, 0, 0, 0};
            NSUInteger length = 6;
            if ( length > pcm.PCMCharacteristic.value.length ) {
                length = pcm.PCMCharacteristic.value.length;
            }
            for ( NSUInteger i = 0; ( i < length ); ++i ) {
                NSRange range = {length - i - 1, 1};
                [pcm.PCMCharacteristic.value getBytes:&b[i] range:range];
            }
            btMacAddress = [NSData dataWithBytes:b length:length];
        }
        return (YES);
    }
    else {
        NSLog(@"Failed: Get BT Mac address; status:%@ - %@", pcm.PCMError.localizedFailureReason,
              pcm.PCMError.localizedDescription);
    }
    
    [self terminateOTAU:@"Failed: Get BT Mac address" :OTAUErrorFailedGetBTMacAddress];
    return (NO);
}

//============================================================================
-(BOOL) getCrystalTrim :(BOOL) isOTAUv5 :(BOOL) isBootMode  {
    PCM *pcm = [self getCsKeyLegacy:isOTAUv5:isBootMode:2];
    if (pcm && (pcm.PCMError == NULL)) {
        crystalTrim = [pcm.PCMCharacteristic.value
                       subdataWithRange:NSMakeRange(0, pcm.PCMCharacteristic.value.length)];
        return (YES);
    }
    else {
        NSLog(@"Failed: Get Crystal Trim; status:%@ - %@", pcm.PCMError.localizedFailureReason,
              pcm.PCMError.localizedDescription);
    }
    
    [self terminateOTAU:@"Failed: Get Crystal Trim" :OTAUErrorFailedGetCrystalTrim];
    return (NO);
}

//============================================================================
// Connect to the targetPeripheral and then discover the characteristics for the given service
-(BOOL) connect:(NSString *) service {
    BOOL    success = NO;
    
    delegateHit=NO;
    [self statusMessage:@"Start: Connect\n"];
    [[Discovery sharedInstance] connectPeripheral:targetPeripheral];
    targetPeripheral.delegate = self;
    PCM *pcm = [self waitForDelegate:kBleDidConnectPeripheral];
    if (pcm) {
        [self statusMessage:@"Success: Connect\n"];
        
        int timeout = 300;
        while (![appDelegate.discoveredChars boolValue] && timeout--) {
            [NSThread sleepForTimeInterval: 0.1];
        }
        if (timeout > 0) {
            success = YES;
            targetPeripheral.delegate = self;
        }
    }
    else {
        [self statusMessage:@"Failed: Connect\n"];
        [self reportError:OTAUErrorFailedConnect];
    }
    
    return (success);
}

    //============================================================================
    // We seem to be in connecting mode, escape from this mode by atempting to disconnect
-(BOOL) stopConnecting {
    BOOL    success = NO;
    
    delegateHit=NO;
    [self statusMessage:@"Start: Stop Connecting \n"];
    [[Discovery sharedInstance] disconnectPeripheral:targetPeripheral];
    targetPeripheral.delegate = self;
    PCM *pcm = [self waitForDelegate:kBleDidDisconnectPeripheral];
    if (pcm) {
        [self statusMessage:@"Success: Disconnected\n"];
        success = YES;
    }
    else {
        [self statusMessage:@"Failed: Disconnect\n"];
        [self reportError:OTAUErrorFailedConnect];
    }
    
    return (success);
}

//============================================================================
// Discover all services for targetPeripheral
-(BOOL) discoverServices {
    delegateHit=NO;
    [self statusMessage:@"Start: Discover Services\n"];
    [targetPeripheral discoverServices:nil];
    
    PCM *pcm = [self waitForDelegate:kDidDiscoverServices];
    if (pcm && ( pcm.PCMError == NULL ) ) {
        [self statusMessage:@"Success: Discover Services\n"];
        appDelegate.peripheralInBoot = [NSNumber numberWithBool: NO];
        for (CBService *service in targetPeripheral.services) {
            if ([service.UUID isEqual:[CBUUID UUIDWithString:serviceBootOtauUuid]]) {
                appDelegate.peripheralInBoot = [NSNumber numberWithBool: YES];
            }
        }
        // Notify delegate that mode changed
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (OTAUDelegate && [OTAUDelegate respondsToSelector: @selector(didChangeMode:)]) {
                [OTAUDelegate didChangeMode: [appDelegate.peripheralInBoot boolValue]];
            }
        }];
        
        return (YES);
    }
    else {
        [self statusMessage:@"Failed: Discover Services\n"];
        [self reportError:OTAUErrorFailedDiscoverServices];
        return (NO);
    }
    
}

//============================================================================
// Setup notification to allow simple flow control when transferring Application
-(BOOL) createNotifyForAppTransfer {
    NSLog(@"otauCreateNotifyForAppTx");
    CBUUID *uuid = [CBUUID UUIDWithString:characteristicTransferControlUuid];
    
    for (CBCharacteristic *characteristic in appDelegate.targetService.characteristics) {
        
        if ([characteristic.UUID isEqual:uuid])
        {
            delegateHit=NO;
            NSLog(@" -SetNotify for %@",characteristic.UUID);
            [targetPeripheral setNotifyValue:YES forCharacteristic:characteristic];
            
            PCM *pcm = [self waitForDelegate:kDidUpdateNotificationStateForCharacteristic];
            if (pcm && ( pcm.PCMError == NULL ) ) {
                return (YES);
            }
            else
                break;
        }
    }
    return (NO);
    
}

//============================================================================
// Write the given data to the given characteristic, providing of course
// that the characteristic has been previously discovered.
-(void) writeCharacteristic:(NSString *)characteristicName withValue:(NSData *) data {
    CBUUID *uuid = [CBUUID UUIDWithString:characteristicName];
    
    for (CBCharacteristic *characteristic in appDelegate.targetService.characteristics) {
        // Is this the characteristic we have discovered
        if ([characteristic.UUID isEqual:uuid]) {
            delegateHit = NO;
            
            [targetPeripheral writeValue:data forCharacteristic:characteristic
                                    type:CBCharacteristicWriteWithResponse];
        }
    }
}



//============================================================================
// Write the given data to the given characteristic, providing of course
// that the characteristic has been previously discovered.
-(NSData *) readCharacteristic:(NSString *)characteristicName {
    CBUUID *uuid = [CBUUID UUIDWithString:characteristicName];
    
    if (appDelegate.targetService.characteristics == nil) {
        [self discoverCharacteristics:[appDelegate.targetService.UUID UUIDString]];
    }
    
    for (CBCharacteristic *characteristic in appDelegate.targetService.characteristics) {
        // Is this the characteristic we have discovered
        if ([characteristic.UUID isEqual:uuid]) {
            delegateHit = NO;
            
            NSLog(@"Request Characteristic %@ Value", characteristicName);
            [self statusMessage:@"Request Characteristic Value\n"];
            delegateHit=NO;
            [targetPeripheral readValueForCharacteristic:characteristic];
            PCM *pcm= [self waitForDelegate:kDidUpdateValueForCharacteristic];
            if (pcm && (pcm.PCMError == NULL)) {
                NSLog(@"Success: Request Characteristic %@ Value", characteristicName);
                [self statusMessage:@"Success: Request Characteristic Value\n"];
                return (pcm.PCMCharacteristic.value);
            }
            else {
                if (pcm.PCMError) {
                    NSLog(@"Failed: Request Characteristic %@ Value, status:%@",
                          characteristicName, pcm.PCMError.localizedDescription);
                }
                [self statusMessage:@"Failed: Request Characteristic Value\n"];
                [self reportError:OTAUErrorFailedRequestChallengeValue];
            }
        }
    }
    return (nil);
}



//============================================================================
// Look for a specific characterisitic from a list of characterisitics
// previously discovered.
-(BOOL) checkForCharacteristic:(NSString *)characteristicName {
    BOOL ret = false;
    CBUUID *uuid = [CBUUID UUIDWithString:characteristicName];
    
    for (CBCharacteristic *characteristic in appDelegate.targetService.characteristics) {
        // Is this the characteristic we have discovered
        if ([characteristic.UUID isEqual:uuid]) {
            ret = true;
            break;
        }
    }
    return ret;
}



//============================================================================
// Set device to boot mode so that we can acces the relevant service for application transfer
// 1. issue the command to Enter Boot Mode, then sleep for a few seconds.
// 2. Once in boot, Device should advertise a new set of services.
// 3. Discover boot mode services and characteristics.
// 4. Return NO if failed to enter boot, otherwise YES.
//
-(BOOL) enterBoot {
    CBUUID *uuid = [CBUUID UUIDWithString:characteristicCurrentAppUuid];
    BOOL success = NO;
    NSUUID  *targetPeripheralID = targetPeripheral.identifier;
    
    // As we expect there will be a disconnect, set waitForDisconnect to avoid a call to Abort.
    waitForDisconnect = YES;
    
    for (CBCharacteristic *characteristic in appDelegate.targetService.characteristics)
    {
        // Is this our characteristic
        if ([characteristic.UUID isEqual:uuid])
        {
            
            delegateHit=NO;
            // Put into Boot Mode
            uint8_t appCommandValue = 0;
            NSData *appCommand      = [NSData dataWithBytes:(void *)&appCommandValue
                                                     length:sizeof(appCommandValue)];
            [targetPeripheral setDelegate:self];
            [targetPeripheral writeValue:appCommand forCharacteristic:characteristic
                                    type:CBCharacteristicWriteWithResponse];
            
            PCM *pcm = [self waitForDelegate:kDidWriteValueForCharacteristic];
            if ( (!pcm) || (pcm.PCMError != NULL) ) {
                if ( pcm ) {
                    NSLog(@" Enter bootmode, write characteristic status: %@", pcm.PCMError.localizedDescription);
                }
                break;
            }
            
            // sleep for some time to allow the device to enter boot and
            // for it to Advertise OTAU Boot service.
            [NSThread sleepForTimeInterval:7];
            
            
            [[Discovery sharedInstance] disconnectPeripheral:targetPeripheral];
            
            delegateHit=NO;
            [[Discovery sharedInstance] setDiscoveryDelegate:self];
            
            
            NSArray *peripherals = [[Discovery sharedInstance]retrievePeripheralsWithIdentifier:targetPeripheralID];
            
            if ([peripherals count]==1)
            {
                targetPeripheral = (CBPeripheral *)[peripherals objectAtIndex:0];
                targetPeripheral.delegate = self;
                
                // connect to the target Peripheral then discover characteristics for the given Service.
                success = [self connect:serviceBootOtauUuid];
                break;
            }
            [[Discovery sharedInstance] setDiscoveryDelegate:nil];
            break;
        }
    }
    
    waitForDisconnect = NO;
    
    return (success);
}



//============================================================================
// Validation of Host by way of encrypted Challenge Value exchange.
//
//
const uint8_t sharedSecretKey[] = {
    0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};

-(BOOL) hostValidation {
    
    if (expectChallengeResponse) {
        [self statusMessage:@"Start: Obtain Challenge Value\n"];
        NSData *challenge = [self readCharacteristic:characteristicDataTransferUuid];
        if (challenge) {
            [self statusMessage:@"Success: Obtain Challenge Value\n"];
            
            NSData *key = [[NSData alloc] initWithBytes:sharedSecretKey length:sizeof(sharedSecretKey)];
            size_t outLength;
            NSMutableData *cipherData = [NSMutableData dataWithLength:challenge.length +
                                         kCCBlockSizeAES128];
            
            CCCryptorStatus result = CCCrypt(kCCEncrypt, // operation
                                             kCCAlgorithmAES128, // Algorithm
                                             kCCOptionPKCS7Padding, // options
                                             key.bytes, // key
                                             key.length, // keylength
                                             nil, // (*iv).bytes,// iv
                                             challenge.bytes, // dataIn
                                             challenge.length, // dataInLength,
                                             cipherData.mutableBytes, // dataOut
                                             cipherData.length, // dataOutAvailable
                                             &outLength); // dataOutMoved
            
            if (result == kCCSuccess) {
                cipherData.length = outLength;
                NSData *destination = [NSData dataWithBytes:((char *)cipherData.bytes) length:16];
                
                [self statusMessage:@"Start: Return Encrypted Challenge Value\n"];
                [self writeCharacteristic:characteristicDataTransferUuid withValue:destination];
                [NSThread sleepForTimeInterval:5];
                [self statusMessage:@"Success: Return Encrypted Challenge Value\n"];
                return (YES);
            }
        }
        [self statusMessage:@"Failed: Obtain Challenge Value\n"];
        [self reportError:OTAUErrorFailedObtainChallengeValue];
        return (NO);
    }
    
    // return YES because challenge response is not expected
    return (YES);
}



//============================================================================
// Transfer Application to target device
// 1. Assume device is in boot mode and Boot services & Characteristics have been discovered.
// 2. Assume challenge-response, if expected, has been completed.
// 3. Transmit new Application image.
// 4. Once complete, switch to the new application and run it.
//
// return NO if there was an Error, otherwise YES.
//
-(BOOL) transferNewAplication:(NSData *) application {
    // Provide a notification path from slave to Host
    
    [self statusMessage:@"Start: Subscribe for Notification\n"];
    if (![self createNotifyForAppTransfer]) {
        [self statusMessage:@"Failed: Subscribe for Notification\n"];
        [self reportError:OTAUErrorFailedSubscribeForNotifications];
        return (NO);
    }
    [self statusMessage:@"Success: Subscribe for Notification\n"];
    
    // Transmit new image
    
    // Write applicationNumber -> kOtaCurAppCharUuid == Select the application to overwrite.
    uint8_t charValue8 = (uint8_t) applicationNumber;
    
    NSData *charValue = [NSData dataWithBytes:(void *)&charValue8
                                       length:sizeof(charValue8)];
    [self writeCharacteristic:characteristicCurrentAppUuid withValue:charValue];
    // Check if the update app image ID was set successfuly
    PCM *pcm = [self waitForDelegate:kDidWriteValueForCharacteristic];
    if ( (!pcm) || (pcm.PCMError != NULL) ) {
        // If there was no error
        if ( pcm && ( pcm.PCMError != NULL ) ) {
            NSLog(@"Setting the update application ID failed: %@, proceeding anyway!", pcm.PCMError.localizedDescription);
        }
        [self statusMessage:@"Warning: Failed to set updated application ID.. proceeding anyway!\n"];
    }
    
    // Write 2 -> kOtaTransCtrlCharUuid == Transfer in progress
    uint16_t charValue16 = 2;
    charValue = [NSData dataWithBytes:(void *)&charValue16
                               length:sizeof(charValue16)];
    [self writeCharacteristic:characteristicTransferControlUuid withValue:charValue];
    // Check if the transfer control was set successfuly
    pcm = [self waitForDelegate:kDidWriteValueForCharacteristic];
    if ( (!pcm) || (pcm.PCMError != NULL) ) {
        if ( pcm ) {
            NSLog(@"Setting app image transer in progress failed: %@", pcm.PCMError.localizedDescription);
        }
        
        [self statusMessage:@"Failed: Set app image transer in progress\n"];
        [self reportError:OTAUErrorFailedToStartOTAUTransfer];
        return (NO);
    }
    

    [self statusMessage:@"Start: Transferring image\n"];
    
    // Application Transfer - Loop
    // TX 20 bytes at a time from the application file.
    transferInProgress = YES;
    transferPercent = 0;
    transferCount = 0;
    int total = (int) [application length];
    transferTotal = (total + 19) / 20;
    int index=0, length=20;
    while (otauRunning == YES && index<total) {
        if ((total-index)<20)
            length=total-index;
        
        NSData *chunkOfAppData = [application subdataWithRange:NSMakeRange(index, length)];
        [self writeCharacteristic:characteristicDataTransferUuid withValue:chunkOfAppData];
        
        do {
            // Wait for notify that characteristic was written.
            PCM *pcm = nil;
            for (int i=0; i<300; i++) {
                usleep(100);
                if (targetPeripheral) {
                    PCM *pcm = [targetPeripheral getCallback];
                    if (pcm) {
                        if ([pcm.PCMName isEqualToString:kDidWriteValueForCharacteristic]) {
                            break;
                        }
                    }
                }
                if (otauRunning==NO)
                    break;
            }
            // If notify was never seen then fail.
            if ( (!pcm) || (pcm.PCMError != NULL)) {
                if ( pcm ) {
                    NSLog(@"Write data failed: %@", pcm.PCMError.localizedDescription);
                }
                break;
            }
        // If the last notify was not for the data transfer characteristic then try again as long as there hasn't been an abort.
        } while (![pcm.PCMCharacteristic.UUID isEqual:[CBUUID UUIDWithString: characteristicDataTransferUuid]] && otauRunning == YES);
        
        index += 20;
    }
    
    // Write 4 -> kOtaTransCtrlCharUuid == Transfer Completed
    charValue16 = 4;
    charValue = [NSData dataWithBytes:(void *)&charValue16
                               length:sizeof(charValue16)];
    [self writeCharacteristic:characteristicTransferControlUuid withValue:charValue];
    // Check if the OTAU was completed successfuly
    if ( (!pcm) || (pcm.PCMError != NULL) ) {
        if ( pcm ) {
            NSLog(@"Failed: while completing OTAU transfer: %@", pcm.PCMError.localizedDescription);
        }
        
        [self statusMessage:@"Failed: while completing OTAU transfer\n"];
        [self reportError:OTAUErrorFailedAppImgCRCCheck];
        return (NO);
    }
    
    return (YES);
}

-(bool) discoverVersion {
    // We want to restore the original value of otau running afterwards.
    bool otauRunningOld = otauRunning;
    otauRunning = YES;
    bool returnVal = NO;
    do {
        // Check the version
        if ( ![appDelegate.peripheralInBoot boolValue] ) {
            // Check if application supports OTAU v5 characteristics (i.e. to look for
            // a possibility that we may have a OTAU v5 bootloader
            if (![self checkForCharacteristic:characteristicGetKeyBlockUuid] || otauRunning == NO) {
                otaVersion = 4;
            }
            else {
                otaVersion = 5;
            }
            
            // hardcoded version
            otaVersion = 5;

            
            returnVal = YES;
        }
        else {
            // Read OTAU bootloader version

#ifdef diagnostic173309
            NSString *str = [NSString stringWithFormat:@"** readValue from %@\n",characteristicVersionUuid];
            [self statusMessage:str];
#endif
            
            NSData* otauVer = [self readCharacteristic:characteristicVersionUuid];
            if ( !otauVer ) {
                [self statusMessage:@"Failed: Read Bootloader Version Characteristics\n"];
                
                // Report a failure and quit
                [self reportError:OTAUErrorFailedReadBootloaderVersion];
                break;
            }
            
            // Extract ht
            [otauVer getBytes:&otaVersion length:sizeof(otaVersion)];
            
            // hardcoded version
            otaVersion = 5;
            
            // When the device switches back to application but doesn't provide GATT status changed
            // indication, iOS still thinks the device is in the bootloader. It could be a reason why
            // we get to this point. In this case the value read into the OTAU version number
            // may possibly be invalid (as the same handle may have been used by something else in
            // the application). So the least we can do is check if we have a valid OTAU bootloader
            // version and report to the user if an invalid valus is found.
            if (otaVersion < 4 || otaVersion > 5)  {
                /* Since we only know of OTAU v4 and v5 at the moment, treat other values as invalid */
                [self statusMessage:[NSString stringWithFormat:@"Failed: Invalid bootloader version specified: %hhu\n", otaVersion]];
                
                // Report a failure and quit
                [self reportError:OTAUErrorFailedReadBootloaderVersion];
                break;
            }
            [self statusMessage:@"Success: Read Bootloader Version Characteristics\n"];
            returnVal = YES;
        }
    } while (0); // one time loop. So that we can use break to terminate instead of Goto.
    // Restore original value.
    otauRunning = otauRunningOld;
    return returnVal;
}

//============================================================================
// Main OTAU function
// 1. Discover characteristics for the Application OTAU Service. If failed attempt
//  discover if bootloader service exists, just to check if the device is already in the boot mode.
// 2. Check if the device has the OTAU v5 services. If that is the case, switch the device to boot mode
//  as we can query the BT address and Crystal trim keys from the OTAU v5 bootloader itself.
// 3. Provide a return path for BT Address and Crystal trim keys by subscribing for notifications on the Data Transfer Characteristic.
// 4. Request Bluetooth MAC address by writing 0 to the CS Key Characteristic.
// 5. Request Crystal Trim by writing 1 to the CS Key Characteristic.
// 6. Prepare the Application file using the keys obtained in step 3 & 4
// 7. If the device is not already in the Bootmode, Switch device to Boot mode and discover Services and Characteristics
// 7a Implement Host Validation
// 8. Transfer the Application from iOS to on-chip.
// 9. Wait for Disconnect - From this it can be inferred that the Application successfuly transferred
//
// Call terminateOTAU if a timeout occurs.
//
-(void) otauMain {
    [self statusMessage:@"Start: Application Update\n"];
    
    otauRunning = YES;
    targetPeripheral.delegate = self;
    
    BOOL completedSuccessfuly = NO;
    
    do {
        // 0. Connect to peripheral, if required
        if (targetPeripheral && targetPeripheral.state == CBPeripheralStateConnecting) {
            // stop connecting
            
            if (![self stopConnecting])
                break;
        }
            
        if (targetPeripheral && targetPeripheral.state != CBPeripheralStateConnected) {
            
            // then discover the characteristics for the given service
            if (![self connect:serviceApplicationOtauUuid]) {
                break;
            }
        }
        
        [OTAUDelegate didUpdateProgress: 5];
         // Get OTA version.
        if (![self discoverVersion]) {
            break;
        }
        [OTAUDelegate didUpdateProgress: 15];
        
        
        /*
         In case app is on boot mode we have to prepare Identity root and Encryption root
         */
        NSString *value = @"0x00000000 00000000 00000000 00000000";
        NSData *d = [value dataUsingEncoding:NSUTF8StringEncoding];
        
        iRoot = d;
        eRoot = d;
        
        // Check if the device is in the application mode e
        if ( ![appDelegate.peripheralInBoot boolValue] ) {
            if (otaVersion >= 5) {
                iRoot = nil;
                eRoot = nil;
                

                // Get IR and ER here before boot loader mode
                [self createNotifyForCSKey];
                iRoot = [self getCsKeyFromBlock:17];
                NSLog(@"Identity root is %@", iRoot);
                eRoot = [self getCsKeyFromBlock:18];
                NSLog(@"Encryption root is %@", eRoot);
                
                if (iRoot == nil || eRoot == nil) {
                    [self statusMessage:@"Failed: Couldn't read identity or encryption root"];
                    break;
                }
                // 1b. If OTAUv5 was used we can query the BT address and Crystal trim
                // from the bootloader itself. So let's switch
                
                // Switch device to Boot mode and discover Services and Characteristics
                [self statusMessage:@"Start: Enter OTA boot loader mode\n"];
                if ( ( [self enterBoot] == NO ) || ( otauRunning == NO ) ) {
                    
                    [self statusMessage:@"Failed: Enter Boot\n"];
                    if (!didAbort) {
                        [self reportError:OTAUErrorFailedEnterBoot];
                    }
                    break;
                }
                
                // 7a Expect Challenge response - Must be done before querying CS Keys
                if ([self hostValidation]==NO) {
                    break;
                }
                //peripheralInBootMode = true;
            }
        }
        
        if (otaVersion == 4) {
            [self statusMessage:@"Start: Subscribe for Notification\n"];
            if (![self createNotifyForCSKey] || otauRunning == NO) {
                [self statusMessage:@"Failed: Subscribe for Notification\n"];
                if (!didAbort) {
                    [self reportError:OTAUErrorFailedSubscribeForNotifications];
                }
                break;
            }
            [self statusMessage:@"Success: Subscribe for Notification\n"];
        }
        
        if ((otaVersion < 5) && (![appDelegate.peripheralInBoot boolValue]) ) {
            iRoot = nil;
            eRoot = nil;
            // Get IR and ER here before boot loader mode
            PCM *pcm = [self getCsKeyLegacy:(otaVersion >= 5) : [appDelegate.peripheralInBoot boolValue] : 17];
            if (pcm && (pcm.PCMError == NULL)) {
                iRoot = pcm.PCMCharacteristic.value;
                NSLog(@"Identity root is %@", iRoot);
            }
            pcm = [self getCsKeyLegacy:(otaVersion >= 5) : [appDelegate.peripheralInBoot boolValue] : 18];
            if (pcm && (pcm.PCMError == NULL)) {
                eRoot = pcm.PCMCharacteristic.value;
                NSLog(@"Encryption root is %@", eRoot);
            }
        
            /* If the device is in application mode and doesn't support OTAU v5, then we need to register for notifications
             * on data transfer characteristic. For OTAUv5 bootloader notifications on data transfer characteristic are
             * not supported */
        }
        else if ((otaVersion < 5) && [appDelegate.peripheralInBoot boolValue]) {
            // We might get here if the device has OTAU v4 bootloader and no valid application
            // In this case we can't proceeed (shouldn't proceed) as we don't have a way to query
            // CS Keys. If we proceed we will fail so stop here.
            
            [self statusMessage:@"Failed: Device is in Bootmode and not using OTAU v5 bootloader. Cannot proceed as CS keys cannot be queried in this configuration.\n"];
            [self reportError:OTAUErrorFailedInvalidConfiguration];
            break;
        }
        // We don't check for OTAU v5 application mode case as we wouldn't get to this point if that was the case.
        
        [OTAUDelegate didUpdateProgress: 20];
        
        // Proceed if the device is in "OTAU v5 bootloader" or "OTAU v4 application"
        
        // 4. Get Bluetooth MAC address by writing 0 to CS Key Characteristic.
        [self statusMessage:@"Start: Read BT Mac address\n"];
        if (![self getBtMacAddress :(otaVersion >= 5) :[appDelegate.peripheralInBoot boolValue]] || otauRunning == NO) {
            [self statusMessage:@"Failed: Read BT Mac address\n"];
            if (!didAbort) {
                [self reportError:OTAUErrorFailedGetBTMacAddress];
            }
            break;
        }
        else {
            // Display BT Mac Address
            if (btMacAddress.length >= 6)
            {
                uint8_t *data = (uint8_t *) [btMacAddress bytes];
                NSString *macAddress = [NSString stringWithFormat:@"Success: Get BT Mac address = %02x:%02x:%02x:%02x:%02x:%02x\n",
                                        *data, *(data+1), *(data+2), *(data+3), *(data+4), *(data+5)];
                [self statusMessage:macAddress];
            }
            [OTAUDelegate didUpdateProgress: 30];
        }
        
        // 5. Get Crystal Trim by writing 1 to CS Key Characteristic.
        [self statusMessage:@"Start: Read Crystal Trim\n"];
        if (![self getCrystalTrim :(otaVersion >= 5) :[appDelegate.peripheralInBoot boolValue]] || otauRunning == NO) {
            [self statusMessage:@"Failed: Read Crystal Trim\n"];
            [self reportError:OTAUErrorFailedGetCrystalTrim];
            break;
        }
        else {
            // Display Crystal trim
            if (crystalTrim.length >= 1) {
                uint8_t *data = (uint8_t *) [crystalTrim bytes];
                [self statusMessage:[NSString stringWithFormat:@"Success: Get Crystal Trim = %d\n", *data]];
            }
            [OTAUDelegate didUpdateProgress: 40];
        }
        
        // 6. Prepare the Application file using these keys
        // Modify Application image file with new value for Crystal Trim and BT Address
        // then generate CRC for data and header blocks.
        [self statusMessage:@"Start: Prepare Application image\n"];
        NSData *newApplication = [[ApplicationImage sharedInstance] prepareFirmwareWith:crystalTrim
                                                                                    and:btMacAddress
                                                                                    and:iRoot
                                                                                    and:eRoot
                                                                                forFile:sourceFilename];
        [self statusMessage:@"Success: Prepare Application image\n"];
        
        [OTAUDelegate didUpdateProgress: 45];
        
        // If the device is not already in the boot-mode
        if (![appDelegate.peripheralInBoot boolValue]) {
            
            // 7. Switch device to Boot mode and discover Services and Characteristics
            [self statusMessage:@"Start: Enter Boot\n"];
            if ([self enterBoot]==NO ||
                otauRunning == NO) {
                [self statusMessage:@"Failed: Enter Boot\n"];
                if (!didAbort) {
                    [self reportError:OTAUErrorFailedEnterBoot];
                }
                break;
            }
            [self statusMessage:@"Success: Enter Boot\n"];
            
            // 7a Expect Challenge response
            if ([self hostValidation]==NO) {
                break;
            }
        }
        
        [OTAUDelegate didUpdateProgress: 50];
        
        // 8. Transfer image file to Device, replacing on chip application.
        if (![self transferNewAplication:newApplication] || otauRunning == NO) {
            [self statusMessage:@"Failed: Transfer image\n"];
            if (!didAbort) {
                [self reportError:OTAUErrorFailedQueueAppTransfer];
            }
            break;
        }
        
        completedSuccessfuly = YES;
        
        // 9. Wait for Disconnect - From this it can be inferred that the Application successfuly transferred
        // Note: There is no timeout, user must use the Abort button to quit the operation
        waitForDisconnect = YES;
        while (waitForDisconnect);
        [NSThread sleepForTimeInterval: 2];
        [self statusMessage:@"Success: Image transferred\n"];
        
        
    } while (0); // one time loop. So that we can use break to terminate instead of Goto.
    
    targetPeripheral.delegate = nil;
    otauRunning = NO;
    
    if (!completedSuccessfuly) {
        if (didAbort) {
            [self terminateOTAU:@"OTA update aborted" :0];
        }
        else {
            [self terminateOTAU:@"Failed: Application Update" :OTAUErrorFailedApplicationUpdate];
        }
    }
    else {
        [self statusMessage:@"Success: Application Update\n\n"];
        [self terminateOTAU:@"Success: Application Update" :0];
    }
}

//============================================================================
// Send status messages to main thread
// This can be used as a progress indicator
-(void) statusMessage:(NSString *) message {
    @synchronized (self) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [OTAUDelegate statusMessage:[message capitalizedString]];
        }];
    }
}


//============================================================================
// inform delegate of Error
-(void) reportError:(NSInteger) errorCode {
    
    // Convert error code to 4 character string
    // this will be used to lookup Error Message
    NSString *errorCodeString = [NSString stringWithFormat:@"%4d",(int)errorCode];
    
    // Lookup Error string from error code
    NSString *errorString = NSLocalizedStringFromTable (errorCodeString, @"ErrorCodes", nil);
    
    // Constrict error object
    NSError *error = [[NSError alloc] initWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                code:errorCode
                                            userInfo:@{errorCodeString: errorString} ];
    
    // Perform callback to Main View (in main thread)
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [OTAUDelegate otauError:error];
    }];
}


/********************************************************************************************/
/*                           BleDiscovery Delegates                                         */
/********************************************************************************************/


- (void) discoveryDidRefresh {}
- (void) discoveryStatePoweredOff {}
- (void) otauPeripheralTest:(CBPeripheral *) peripheral :(BOOL) isOtau {}
- (void) didDiscoverServices:(CBPeripheral *) peripheral {}
- (void) centralPoweredOn {}

-(void) didChangeMode {
    // Notify OTAU delegate that Boot/App mode changed
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if (OTAUDelegate && [OTAUDelegate respondsToSelector: @selector(didChangeMode:)]) {
            [OTAUDelegate didChangeMode: [appDelegate.peripheralInBoot boolValue]];
        }
    }];
}


@end
