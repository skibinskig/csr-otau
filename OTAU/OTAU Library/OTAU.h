//
//  OTAU.h
//  OTAU
//
/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/
//
// Over-The-Air-Update Process
//
#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "Discovery.h"

/****************************************************************************/
/*						OTAU Service for Boot & Application					*/
/****************************************************************************/
// service UUID exposed by the Boot module
#define     serviceBootOtauUuid     @"00001010-d102-11e1-9b23-00025b00a5a5"

/****************************************************************************/
/*				 Boot Loader Service and Characteristics                    */
/****************************************************************************/
// Service
#define characteristicVersionUuid   @"00001011-d102-11e1-9b23-00025b00a5a5"

//  - Characteristic : Application number to be updated
// Write 0 to switch Application to Boot Loader
// Write 1 for Application 1, 2 for Application 2 etc.
#define characteristicCurrentAppUuid    @"00001013-d102-11e1-9b23-00025b00a5a5"


// Data transfer char UUID
// Set up this char for notifications
#define characteristicDataTransferUuid  @"00001014-d102-11e1-9b23-00025b00a5a5"


// Transfer control char UUID
// Setup this for Notifications
// 1 - Ready
// 2-Transfer In Progress
// 3-Transfer Paused
// 4-Transfer Complete
// 5-Transfer failed
// 6-Transfer Aborted
#define characteristicTransferControlUuid @"00001015-d102-11e1-9b23-00025b00a5a5"

/****************************************************************************/
/* Application Service and characteristics related to OTAU                  */
/****************************************************************************/
// This service exposes Over-the-Air update capabilities of CSR μEnergy applications.
#define serviceApplicationOtauUuid  @"00001016-d102-11e1-9b23-00025b00a5a5"


// Read CS-key char UUID
// Subscribe to kOtaDataTransCharUuid
// Write 1 for Bluetooth Mac address
// Write 2 for Crystal Trim
// Notification will be received on kOtaDataTransCharUuid
#define characteristicGetKeysUuid @"00001017-d102-11e1-9b23-00025b00a5a5"

// Read CS-key block char UUID
// Only used to see if the application supports OTAU v5 library
// This way we determine if we expect to see a OTAU v5 bootloader
// which can be directly queried for relevant CS Keys
#define characteristicGetKeyBlockUuid @"00001018-d102-11e1-9b23-00025b00a5a5"


/****************************************************************************/
/*                  Error reporting typedefs                                */
/****************************************************************************/
typedef enum OtauErrorTypes {
    OTAUErrorFailedConnect = 1000,                          // 1000
    OTAUErrorFailedDiscoverServices,                        // 1001
    OTAUErrorFailedRequestChallengeValue,                   // 1002
    OTAUErrorFailedObtainChallengeValue,                    // 1003
    OTAUErrorFailedSubscribeForNotifications,               // 1004
    OTAUErrorFailedDiscoverApplicationCharacteristics,      // 1005
    OTAUErrorFailedGetBTMacAddress,                         // 1006
    OTAUErrorFailedGetCrystalTrim,                          // 1007
    OTAUErrorFailedEnterBoot,                               // 1008
    OTAUErrorFailedQueueAppTransfer,                        // 1009
    OTAUErrorFailedApplicationUpdate,                       // 1010
    OTAUErrorFailedDisconnected,                            // 1011
    OTAUErrorFailedReadBootloaderVersion,                   // 1012
    OTAUErrorFailedDiscoverBootloaderCharacteristics,       // 1013
    OTAUErrorFailedInvalidConfiguration,                    // 1014
    OTAUErrorFailedToStartOTAUTransfer,                     // 1015
    OTAUErrorFailedAppImgFragmentTransfer,                  // 1016
    OTAUErrorFailedAppImgCRCCheck,                          // 1017
    OTAUErrorFailedNotInitialised,                          // 1018
} OtauErrors;

/****************************************************************************/
/*						OTAU Class Delegates                                */
/****************************************************y************************/
@protocol OTAUDelegate <NSObject>
@optional
-(void) didUpdateProgress: (uint8_t) percent;
-(void) completed:(NSString *) message :(NSError *) error;
-(void) statusMessage:(NSString *)message;
-(void) otauError:(NSError *) error;
-(void) initComplete: (uint8_t) otauVersion;
-(void) didGetCsKeyValue: (NSData *) value;
-(void) didChangeConnectionState: (bool) isConnected;
-(void) didChangeMode: (bool) isBootMode;
@end


/****************************************************************************/
/*						OTAU Class Interface                                */
/****************************************************************************/
@interface OTAU : NSObject <CBPeripheralDelegate, DiscoveryDelegate>


@property (strong, nonatomic) id<OTAUDelegate> OTAUDelegate;

+ (id) sharedInstance;

-(void) startOTAU: (NSString *) firmwareFilepath and:(BOOL) challengeResponseEnabled;

-(void) abortOTAU: (CBPeripheral *) peripheral;

-(void) reportError: (NSInteger) errorCode;

-(bool) parseCsKeyJson: (NSString*) csKeyJsonFile;

-(void) initOTAU: (CBPeripheral *) peripheral;

-(void) getCsKey: (uint8_t) csKeyIndex;
@end
