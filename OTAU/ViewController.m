//
//  ViewController.m
//  OTAU
//
/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/
//

#import "ViewController.h"
#import "AppDelegate.h"

static const uint8_t CS_KEY_BDADDR = 1;
static const uint8_t CS_KEY_XTAL_TRIM = 2;

@interface ViewController ()
@property BOOL isUpdateRunning;
@property BOOL isInitRunning;
@property BOOL isAbortButton;
@property uint8_t expectedCsKey;
@end

@implementation ViewController

@synthesize firmwareName, targetName, updateButtonName;
@synthesize firmwareFilename, targetPeripheral;
@synthesize isAbortButton, isUpdateRunning, isInitRunning, expectedCsKey;

@synthesize deviceAddress, connectionState, crystalTrim, fwVersion;
@synthesize statusLog;
@synthesize startButton, abortButton, progressBar;
@synthesize challengeResponseSwitch;
@synthesize modeLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [[OTAU sharedInstance] setOTAUDelegate:self];
    
    [self statusMessage: @"Start: Load CS key JSON\n"];
    if ([[OTAU sharedInstance] parseCsKeyJson:@"cskey_db"]) {
        [self statusMessage: @"Success: Load CS key JSON\n"];
    }
    else {
        [self statusMessage: @"Fail: Load CS key JSON\n"];
    }

    [connectionState setText:@"DISCONNECTED"];
    
    progressBar.progress = 0.0;
    isInitRunning = NO;
    
    // Did we open App with email or dropbox attachment?
    AppDelegate *appDelegate= (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.urlImageFile)
        [self handleOpenURL:appDelegate.urlImageFile];
}

- (void)viewDidAppear:(BOOL)animated {
    AppDelegate *appDelegate= (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate.peripheralInBoot boolValue]==YES) {
        [modeLabel setText:@"BOOT"];
    }
    else {
        [modeLabel setText:@"APP"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"chooseFirmware"]) {
        FirmwareSelector *fs = (FirmwareSelector *)[segue destinationViewController];
        fs.firmwareDelegate = self;
    }

    if ([[segue identifier] isEqualToString:@"chooseTarget"]) {
        DiscoverViewController *dvc = (DiscoverViewController *)[segue destinationViewController];
        dvc.discoveryViewDelegate = self;
    }
}



// Delegates
-(void) firmwareSelector:(NSString *) filepath {
    if ([filepath isEqualToString:@""]) {
    }
    else {
        NSString *filename = [[filepath lastPathComponent] stringByDeletingPathExtension];
        [firmwareName setTitle:filename forState:UIControlStateNormal];
        [firmwareName setAlpha:1.0];
        firmwareFilename = filepath;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    [self setStartAndAbortButtonLook];
}

-(void) setTarget:(id)peripheral
{
    if (peripheral != nil)
    {
        [targetName setTitle:[peripheral name] forState:UIControlStateNormal];
        [targetName setAlpha:1.0];
        targetPeripheral = peripheral;
        [connectionState setText:@"CONNECTED"];
        isInitRunning = YES;
        [self setStartAndAbortButtonLook];
        [[OTAU sharedInstance] initOTAU:peripheral];
    }
    
    // Dismiss Discovery View and any nulify all delegates set up for it.
    [[Discovery sharedInstance] setDiscoveryDelegate:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Discovery sharedInstance] stopScanning];
    [self setStartAndAbortButtonLook];
}

- (IBAction)cancelFirmware:(id)sender
{
    [firmwareName setTitle:@"Set Firmware" forState:UIControlStateNormal];
    [firmwareName setAlpha:0.4];
    firmwareFilename=nil;
    [self setStartAndAbortButtonLook];
}

- (IBAction)cancelTarget:(id)sender
{
    [targetName setTitle:@"Set Target" forState:UIControlStateNormal];
    [targetName setAlpha:0.4];
    targetPeripheral=nil;
    [self setStartAndAbortButtonLook];
}

//============================================================================
// Called when the Start button is pressed
// 
- (IBAction)startUpdate:(id)sender
{
    if (isAbortButton==NO)
    {
        if (firmwareFilename!=nil && targetPeripheral!=nil)
        {
            isAbortButton=YES;
            isUpdateRunning=YES;
            [self statusMessage:@"------[ Update Started ]------\n"];
            [[OTAU sharedInstance] startOTAU:firmwareFilename and:(challengeResponseSwitch.on)];
            self.progressBar.progress = 0.0;
            [self setStartAndAbortButtonLook];
        }
    }
}

//============================================================================
// Called when the Abort button is pressed
//
- (IBAction)abortUpdate:(id)sender
{
    if (isAbortButton==YES)
    {
        isAbortButton=NO;
        [self statusMessage:@"Update Aborted\n"];
        [[OTAU sharedInstance]  abortOTAU:targetPeripheral];
        isUpdateRunning=NO;
        [self setStartAndAbortButtonLook];
    }
}

//============================================================================
// Set the look of the Start and Abort Buttons to reflect whether or not the button is available.
//
// Call this method after...
//  - Setting the App File
//  - Choosing the target
//  - After Start Button is Pressed
//  - After Abort button is pressed
//  - At the end of an Update
//  - Update error received.
//
// To Disable button: set Alpha to 0.2 and Enabled to NO
// To Enable Button:  set Alpha to 1.0 and Enabled to YES
//
// Enable Start Button if App File and Target are both valid and update not running.
// Enable Abort Button if Update is running.
-(void) setStartAndAbortButtonLook
{
    if (isInitRunning || isUpdateRunning) {
        [startButton setEnabled:NO];
        [firmwareName setEnabled:NO];
        [targetName setEnabled:NO];
        [challengeResponseSwitch setEnabled:NO];
    }
    else {
        [firmwareName setEnabled:YES];
        [targetName setEnabled:YES];
        [challengeResponseSwitch setEnabled:YES];
        if (targetPeripheral!=nil && firmwareFilename!=nil)
        {
            [startButton setEnabled:YES];
        }
        else
        {
            [startButton setEnabled:NO];
        }
    }
    
    if (isUpdateRunning)
    {
        [abortButton setHidden:NO];
        [progressBar setHidden:NO];
        [abortButton setEnabled:YES];
        [startButton setHidden:YES];
    }
    else
    {
        [abortButton setHidden:YES];
        [abortButton setEnabled:NO];
        [progressBar setHidden:YES];
        [startButton setHidden:NO];
    }
}


/****************************************************************************/
/*								Open With.....                              */
/****************************************************************************/
-(void) handleOpenURL:(NSURL *)url
{
    //NSError *outError;
    //NSString *fileString = [NSString stringWithContentsOfURL:url
     //                                               encoding:NSUTF8StringEncoding
       //                                                error:&outError];
    NSString *filename = [[url lastPathComponent] stringByDeletingPathExtension];
    [firmwareName setTitle:filename forState:UIControlStateNormal];
    [firmwareName setAlpha:1.0];
    firmwareFilename = url.path;
    [self statusMessage:[NSString stringWithFormat:@"Imported File %@\n",firmwareFilename]];
}



/****************************************************************************/
/*								Delegates                                   */
/****************************************************************************/
//============================================================================
// Update the progress bar when a progress percentage update is received during OTAU update.
-(void) didUpdateProgress: (uint8_t) percent {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        self.progressBar.progress = percent / 100.0f;
    }];
}

//============================================================================
// This delegate is called after we have called initOTAU and the library has finished
// querying the peripheral.
-(void) initComplete: (uint8_t) otauVersion {
    [self.deviceAddress setText: @"-"];
    [self.crystalTrim setText: @"-"];
    [self.fwVersion setText: @"-"];
    
    if (otauVersion > 3) {
        [fwVersion setText: [NSString stringWithFormat:@"%d", otauVersion]];
        [self statusMessage: @"Start: Read BT address and trim\n"];
        expectedCsKey = CS_KEY_BDADDR;
        [[OTAU sharedInstance] getCsKey: CS_KEY_BDADDR];
    }
    else {
        [self statusMessage: @"Failed to read OTAU version\n"];
        isInitRunning = NO;
        [self setStartAndAbortButtonLook];
    }
}

//============================================================================
// This delegate is called when the selected peripheral connection state changes.
-(void) didChangeConnectionState:(bool)isConnected {
    if (isConnected) {
        [connectionState setText:@"CONNECTED"];
    }
    else {
        [connectionState setText:@"DISCONNECTED"];
    }
}

//============================================================================
-(void) didGetCsKeyValue: (NSData *) value {
    if (value == nil) {
        [self statusMessage: @"Failed to read BT address and trim\n"];
        isInitRunning = NO;
        [self setStartAndAbortButtonLook];
    }
    else {
        if (expectedCsKey == CS_KEY_BDADDR) {
            uint8_t *octets = (uint8_t*)[value bytes];
            NSString *display = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                                 octets[5], octets[4], octets[3], octets[2], octets[1], octets[0] ];
            
            [deviceAddress setText:display];
            expectedCsKey = CS_KEY_XTAL_TRIM;
            [self statusMessage: @"didGetCsKey expectedCsKey == CS_KEY_BDADDR\n"];
            [[OTAU sharedInstance] getCsKey: expectedCsKey];
        }
        else if (expectedCsKey == CS_KEY_XTAL_TRIM) {
            const uint16_t *trim = (uint16_t*)[value bytes];
            NSString *display = [NSString stringWithFormat:@"0x%X", *trim];
            [crystalTrim setText:display];
            [self statusMessage: @"Success: Read BT address and trim\n"];
            isInitRunning = NO;
            [self setStartAndAbortButtonLook];
        }
        else {
            [self setStartAndAbortButtonLook];
            [self statusMessage: @"expectedCSKey invalid state\n"];
        }
    }
    [self statusMessage: @"didGetCsKey was called\n"];
}

-(void) didChangeMode: (bool) isBootMode {
    if (isBootMode) {
        [modeLabel setText: @"BOOT"];
    }
    else {
        [modeLabel setText: @"APP"];
    }
}

//============================================================================
// Display an Alert upon completion
// Only Alert user if completed successfuly, as the alert for the error will have already been sent
-(void) completed:(NSString *) message :(NSError *) error {
    isAbortButton=NO;
    isUpdateRunning=NO;
    [self setStartAndAbortButtonLook];

    NSString *title;
    
    if (error ==nil) {
        title = @"OTAU";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

//============================================================================
// Display the status in the Text view
-(void) statusMessage:(NSString *)message
{
    [statusLog setText:[statusLog.text stringByAppendingString:message]];
    NSRange range = NSMakeRange(statusLog.text.length - 1, 1);
    [statusLog scrollRangeToVisible:range];
 }


//============================================================================
// Display error as an Alert
-(void) otauError:(NSError *) error {
    
    // Convert error code to 4 character string, as error codes will be in the range 1000-9999
    NSString *errorCodeString = [NSString stringWithFormat:@"%4d",(int)error.code];
    
    // Lookup Error string from error code
    NSString *errorString = NSLocalizedStringFromTable (errorCodeString, @"ErrorCodes", nil);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OTAU Error"
                                                    message:errorString
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


@end
