//
//  ViewController.h
//  OTAU
//
/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/
//
//  Main View
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "FirmwareSelector.h"
#import "DiscoverViewController.h"
#import "OTAU.h"

@interface ViewController : UIViewController <FirmwareDelegate, DiscoveryViewDelegate, OTAUDelegate>

@property (weak, nonatomic) IBOutlet UIButton *firmwareName;
@property (weak, nonatomic) IBOutlet UIButton *targetName;
@property (weak, nonatomic) IBOutlet UIButton *updateButtonName;

@property (strong, nonatomic) NSString *firmwareFilename;
@property (strong, nonatomic) CBPeripheral *targetPeripheral;

@property (weak, nonatomic) IBOutlet UITextView *statusLog;

-(void) handleOpenURL:(NSURL *)url;
- (IBAction)startUpdate:(id)sender;
- (IBAction)abortUpdate:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *abortButton;
@property (weak, nonatomic) IBOutlet UISwitch *challengeResponseSwitch;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UILabel *deviceAddress;
@property (weak, nonatomic) IBOutlet UILabel *connectionState;
@property (weak, nonatomic) IBOutlet UILabel *crystalTrim;
@property (weak, nonatomic) IBOutlet UILabel *fwVersion;
@property (weak, nonatomic) IBOutlet UILabel *modeLabel;

@end
